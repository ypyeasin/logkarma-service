import Home from './../application/controllers/Home'

// const Home = require('./../application/controllers/Home');
// console.log( Home );
export default (app) => {
        
    const homeController = new Home;
    app.get('/', homeController.index);
    app.get('/test', homeController.test);
    app.post('/test', homeController.testPost);

}