import ResourceController from './../../bin/ResourceController'

export default class Home extends ResourceController{
    constructor(){
        super();
        console.log('Controller Constructor');
    }

    index(req, res){
        res.send('hello world from home controller');
    }

    test(req, res){
        res.send('hello world from test controller');
    }

    testPost(req, res){
        res.send('hello world from test post controller');
    }
}