var express = require('express')
var app = express()

import RouteResolver from './bin/RouteResolver'

RouteResolver(app);

app.listen(3000, () => console.log('Example app listening on port 3000!'));