const path = require('path');

module.exports = {
    entry: './app.js',
    target: 'node',
    node: {
        fs: "empty",
        net: 'empty'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ["es2015", "stage-2"]
                    }
                }
            }
        ]
    }
};